﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WMS_RFID.Models
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public Byte[] TmpPassword { get; set; }
        public bool Active { get; set; }
        public int UserRoleID { get; set; }
        public string PlantCode { get; set; }
        public DateTime LastLogAt { get; set; }
        public string LastLogIP { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
