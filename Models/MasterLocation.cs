﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WMS_RFID.Models
{
    public class MasterLocation
    {
        public int Id { get; set; }
        public int WarehouseID { get; set; }
        public string FactoryID { get; set; }
        public string LocationName { get; set; }
        public string RFIDTag { get; set; }
        public string Remarks { get; set; }
        public bool IsDefault { get; set; }
        public string UserID { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
