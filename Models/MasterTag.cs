﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WMS_RFID.Models
{
    public class MasterTag
    {
        public int Id { get; set; }
        public string RfidTag { get; set; }
        public char TagType { get; set; }
        public bool IsAvailable { get; set; }
        public string UserID { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
