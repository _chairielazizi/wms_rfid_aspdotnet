﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WMS_RFID.Models
{
    public class MasterWarehouse
    {
        public int Id { get; set; }
        public string FactoryID { get; set; }
        public string CompanyName { get; set; }
        public string WarehouseName { get; set; }
        public string GroupFactory { get; set; }
        public string SAPPlantCode { get; set; }
        public string UserID { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
