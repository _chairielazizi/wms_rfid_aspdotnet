﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WMS_RFID.Models
{
    public class MasterSAPVendorCode
    {
        public int Id { get; set; }
        public string LocationName { get; set; }
        
        [Key]
        public string VendorCode { get; set; }

        public string VendorType { get; set; }
        public string CompanyName { get; set; }
        public bool Active { get; set; }
        public string UserID { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
