﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WMS_RFID.Models
{
    public class MasterScanner
    {
        public int Id { get; set; }
        public string Warehouse { get; set; }
        public string Name { get; set; }
        public string MACAddress { get; set; }
        public string LastUser { get; set; }
        public DateTime LastUsedAt { get; set; }
        public string UserID { get; set; }
        public bool Active { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
