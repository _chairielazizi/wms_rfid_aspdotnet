﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WMS_RFID.Models
{
    public class MasterSAPSetting
    {
        public int Id { get; set; }
        public string ProgramName { get; set; }
        public string AppServerHost { get; set; }
        public string SystemNumber { get; set; }
        public string SystemID { get; set; }
        public string SAPUser { get; set; }
        public string Password { get; set; }
        public string Client { get; set; }
        public string Language { get; set; }
        public string PoolSize { get; set; }
        public string MaxPoolSize { get; set; }
        public string IdleTimeOut { get; set; }
        public string ServerRestartTime { get; set; }
        public string UserID { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
