﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS_RFID.Models;
using WMS_RFID.Repositories;

namespace WMS_RFID.IRepositories
{
    public interface IMasterSAPVendorCodeRepository : IGenericRepository<MasterSAPVendorCode>
    {
    }
}
