﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS_RFID.Models;

namespace WMS_RFID.IRepositories
{
    public interface IUserRepository : IGenericRepository<User>
    {
    }
}
