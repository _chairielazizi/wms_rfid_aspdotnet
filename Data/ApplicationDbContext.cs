﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS_RFID.Models;

namespace WMS_RFID.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        //public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<MasterScanner> MasterScanner { get; set; }
        public virtual DbSet<MasterLocation> MasterLocation { get; set; }
        public virtual DbSet<MasterProgram> MasterProgram {get;set;}
        public virtual DbSet<MasterSAPSetting> MasterSAPSetting{ get; set; }
        public virtual DbSet<MasterSAPStorageLocation> MasterSAPStorageLocation { get; set; }
        public virtual DbSet<MasterSAPVendorCode>  MasterSAPVendorCode{ get; set; }
        public virtual DbSet<MasterTag> MasterTag{ get; set; }
        public virtual DbSet<MasterWarehouse> MasterWarehouse { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
