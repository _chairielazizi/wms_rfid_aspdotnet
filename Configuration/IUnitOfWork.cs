﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS_RFID.IRepositories;

namespace WMS_RFID.Configuration
{
    public interface IUnitOfWork
    {
        IUserRepository User { get; }
        IMasterScannerRepository MasterScanner { get; }
        IMasterLocationRepository MasterLocation { get; }
        IMasterProgramRepository MasterProgram { get; }
        IMasterSAPSettingRepository MasterSAPSetting { get; }
        IMasterSAPStorageLocationRepository MasterSAPStorageLocation { get; }
        IMasterSAPVendorCodeRepository MasterSAPVendorCode { get; }
        IMasterTagRepository MasterTag { get; }
        IMasterWarehouseRepository MasterWarehouse { get; }
        Task CompleteAsync();
        void Dispose();
    }
}
