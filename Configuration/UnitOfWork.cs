﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS_RFID.Data;
using WMS_RFID.IRepositories;
using WMS_RFID.Repositories;

namespace WMS_RFID.Configuration
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger _logger;

        public IUserRepository User { get; private set; }
        public IMasterScannerRepository MasterScanner { get; private set; }
        public IMasterLocationRepository MasterLocation { get; private set; }
        public IMasterProgramRepository MasterProgram { get; private set; }
        public IMasterSAPSettingRepository MasterSAPSetting { get; private set; }
        public IMasterSAPStorageLocationRepository MasterSAPStorageLocation { get; private set; }
        public IMasterSAPVendorCodeRepository MasterSAPVendorCode { get; private set; }
        public IMasterTagRepository MasterTag { get; private set; }
        public IMasterWarehouseRepository MasterWarehouse { get; private set; }

        public UnitOfWork(ApplicationDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("logs");

            User = new UserRepository(context, _logger);
            MasterScanner = new MasterScannerRepository(context, _logger);
            MasterLocation = new MasterLocationRepository(context, _logger);
            MasterProgram = new MasterProgramRepository(context, _logger);
            MasterSAPSetting = new MasterSAPSettingRepository(context, _logger);
            MasterSAPStorageLocation = new MasterSAPStorageLocationRepository(context, _logger);
            MasterSAPVendorCode = new MasterSAPVendorCodeRepository(context, _logger);
            MasterTag = new MasterTagRepository(context, _logger);
            MasterWarehouse = new MasterWarehouseRepository(context, _logger);
        }

        public async Task CompleteAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
