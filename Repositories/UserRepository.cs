﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS_RFID.Data;
using WMS_RFID.IRepositories;
using WMS_RFID.Models;

namespace WMS_RFID.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(ApplicationDbContext context, ILogger logger) : base(context, logger)
        {

        }

        public override async Task<IEnumerable<User>> All()
        {
            try
            {
                return await dbSet.ToListAsync();
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} All function error", typeof(UserRepository));
                return new List<User>();
            }
        }

        public override async Task<bool> Upsert(User entity)
        {
            try
            {
                var existingUser = await dbSet.Where(x => x.Id == entity.Id).FirstOrDefaultAsync();

                if (existingUser == null)
                    return await Add(entity);

                existingUser.FirstName = entity.FirstName;
                existingUser.LastName = entity.LastName;
                existingUser.Email = entity.Email;
                existingUser.UserName = entity.UserName;
                existingUser.Password = entity.Password;
                existingUser.TmpPassword = entity.TmpPassword;
                existingUser.Active = entity.Active;
                existingUser.UserRoleID = entity.UserRoleID;
                existingUser.PlantCode = entity.PlantCode;
                existingUser.LastLogAt = entity.LastLogAt;
                existingUser.LastLogIP = entity.LastLogIP;
                existingUser.CreatedAt = entity.CreatedAt;
                existingUser.UpdatedAt = entity.UpdatedAt;

                return true;
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} Upsert function error", typeof(UserRepository));
                return false;
            }
        }

        public override async Task<bool> Delete(int id)
        {
            try
            {
                var exist = await dbSet.Where(x => x.Id == id).FirstOrDefaultAsync();

                if (exist == null) return false;

                dbSet.Remove(exist);
                return true;
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} Delete function error", typeof(UserRepository));
                return false;
            }
        }
    }
}
