﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS_RFID.Data;
using WMS_RFID.IRepositories;
using WMS_RFID.Models;

namespace WMS_RFID.Repositories
{
    public class MasterTagRepository : GenericRepository<MasterTag>, IMasterTagRepository
    {
        public MasterTagRepository(ApplicationDbContext context, ILogger _logger) : base(context, _logger)
        {

        }

        public override async Task<IEnumerable<MasterTag>> All()
        {
            try
            {
                return await dbSet.ToListAsync();
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} All function error", typeof(MasterTagRepository));
                return new List<MasterTag>();
            }
        }

        public override async Task<bool> Upsert(MasterTag entity)
        {
            try
            {
                var existingTag = await dbSet.Where(x => x.Id == entity.Id).FirstOrDefaultAsync();

                if (existingTag == null)
                    return await Add(entity);

                existingTag.RfidTag = entity.RfidTag;
                existingTag.TagType = entity.TagType;
                existingTag.IsAvailable = entity.IsAvailable;
                existingTag.UserID = entity.UserID;
                existingTag.CreatedAt = entity.CreatedAt;
                existingTag.UpdatedAt = entity.UpdatedAt;

                return true;
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} Upsert function error", typeof(MasterTagRepository));
                return false;
            }
        }

        public override async Task<bool> Delete(int id)
        {
            try
            {
                var exist = await dbSet.Where(x => x.Id == id).FirstOrDefaultAsync();

                if (exist == null) return false;

                dbSet.Remove(exist);
                return true;   
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} Delete function error", typeof(MasterTagRepository));
                return false;
            }
        }
    }
}
