﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS_RFID.Data;
using WMS_RFID.IRepositories;
using WMS_RFID.Models;

namespace WMS_RFID.Repositories
{
    public class MasterProgramRepository : GenericRepository<MasterProgram>, IMasterProgramRepository 
    {
        public MasterProgramRepository(ApplicationDbContext context, ILogger logger) : base(context, logger)
        {

        }

        public override async Task<IEnumerable<MasterProgram>> All()
        {
            try
            {
                return await dbSet.ToListAsync();
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} All function error", typeof(MasterProgramRepository));
                return new List<MasterProgram>();
            }
        }

        public override async Task<bool> Upsert(MasterProgram entity)
        {
            try
            {
                var existingProgram = await dbSet.Where(x => x.Id == entity.Id).FirstOrDefaultAsync();

                if (existingProgram == null)
                    return await Add(entity);

                existingProgram.Code = entity.Code;
                existingProgram.Name = entity.Name;
                existingProgram.Version = entity.Version;
                existingProgram.Category = entity.Category;

                return true;
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} Upsert function error", typeof(MasterProgramRepository));
                return false;
            }
        }

        public override async Task<bool> Delete(int id)
        {
            try
            {
                var exist = await dbSet.Where(x => x.Id == id).FirstOrDefaultAsync();
                
                if (exist == null) return false;

                dbSet.Remove(exist);
                return true;
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} Delete function error", typeof(MasterProgramRepository));
                return false;
            }
        }
    }
}
