﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS_RFID.Data;
using WMS_RFID.IRepositories;
using WMS_RFID.Models;

namespace WMS_RFID.Repositories
{
    public class MasterLocationRepository : GenericRepository<MasterLocation> , IMasterLocationRepository
    {
        public MasterLocationRepository(ApplicationDbContext context,ILogger logger) : base(context, logger)
        {

        }

        public override async Task<IEnumerable<MasterLocation>> All()
        {
            try
            {
                return await dbSet.ToListAsync();
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} All function error", typeof(MasterLocationRepository));
                return new List<MasterLocation>();
            }
        }

        public override async Task<bool> Upsert(MasterLocation entity)
        {
            try
            {
                var existingLocation = await dbSet.Where(x => x.Id == entity.Id).FirstOrDefaultAsync();

                if (existingLocation == null)
                    return await Add(entity);

                existingLocation.WarehouseID = entity.WarehouseID;
                existingLocation.FactoryID = entity.FactoryID;
                existingLocation.LocationName = entity.LocationName;
                existingLocation.RFIDTag = entity.RFIDTag;
                existingLocation.Remarks = entity.Remarks;
                existingLocation.IsDefault = entity.IsDefault;
                existingLocation.UserID = entity.UserID;
                existingLocation.UpdatedAt = entity.UpdatedAt;
                existingLocation.CreatedAt = entity.CreatedAt;

                return true;
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} Upsert function error", typeof(MasterLocationRepository));
                return false;
            }
        }

        public override async Task<bool> Delete(int id)
        {
            try
            {
                var exist = await dbSet.Where(x => x.Id == id).FirstOrDefaultAsync();

                if (exist == null) return false;

                dbSet.Remove(exist);
                return true;
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} Delete function error", typeof(MasterLocationRepository));
                return false;
            }
        }
    }
}
