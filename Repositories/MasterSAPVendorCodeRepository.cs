﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS_RFID.Data;
using WMS_RFID.IRepositories;
using WMS_RFID.Models;

namespace WMS_RFID.Repositories
{
    public class MasterSAPVendorCodeRepository : GenericRepository<MasterSAPVendorCode>, IMasterSAPVendorCodeRepository
    {
        public MasterSAPVendorCodeRepository(ApplicationDbContext context, ILogger _logger) : base(context, _logger)
        {

        }

        public override async Task<IEnumerable<MasterSAPVendorCode>> All()
        {
            try
            {
                return await dbSet.ToListAsync();
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} All function error", typeof(MasterSAPVendorCodeRepository));
                return new List<MasterSAPVendorCode>();
            }
        }

        public override async Task<bool> Upsert(MasterSAPVendorCode entity)
        {
            try
            {
                var existingVendor = await dbSet.Where(x => x.Id == entity.Id).FirstOrDefaultAsync();

                if (existingVendor == null)
                    return await Add(entity);

                existingVendor.LocationName = entity.LocationName;
                existingVendor.VendorCode = entity.VendorCode;
                existingVendor.VendorType = entity.VendorType;
                existingVendor.CompanyName = entity.CompanyName;
                existingVendor.Active = entity.Active;
                existingVendor.UserID = entity.UserID;
                existingVendor.CreatedAt = entity.CreatedAt;
                existingVendor.UpdatedAt = entity.UpdatedAt;

                return true;
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} Upsert function error", typeof(MasterSAPVendorCodeRepository));
                return false;
            }
        }

        public override async Task<bool> Delete(int id)
        {
            try
            {
                var exist = await dbSet.Where(x => x.Id == id).FirstOrDefaultAsync();

                if (exist == null) return false;

                dbSet.Remove(exist);
                return true;
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} Delete function error", typeof(MasterSAPVendorCodeRepository));
                return false;
            }
        }
    }
}
