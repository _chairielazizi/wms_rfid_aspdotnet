﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS_RFID.Data;
using WMS_RFID.IRepositories;
using WMS_RFID.Models;

namespace WMS_RFID.Repositories
{
    public class MasterScannerRepository : GenericRepository<MasterScanner>, IMasterScannerRepository
    {
        public MasterScannerRepository(ApplicationDbContext context, ILogger logger) : base(context, logger)
        {

        }

        public override async Task<IEnumerable<MasterScanner>> All()
        {
            try
            {
                return await dbSet.ToListAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "{Repo} All function error", typeof(MasterScannerRepository));
                return new List<MasterScanner>();
            }
        }

        public override async Task<bool> Upsert(MasterScanner entity)
        {
            try
            {
                var existingScanner = await dbSet.Where(x => x.Id == entity.Id).FirstOrDefaultAsync();

                if (existingScanner == null)
                    return await Add(entity);

                existingScanner.Warehouse = entity.Warehouse;
                existingScanner.Name = entity.Name;
                existingScanner.MACAddress = entity.MACAddress;
                existingScanner.LastUser = entity.LastUser;
                existingScanner.LastUsedAt = entity.LastUsedAt;
                existingScanner.UserID = entity.UserID;
                existingScanner.Active = entity.Active;
                existingScanner.Active = entity.Active;
                existingScanner.CreatedAt = entity.CreatedAt;
                existingScanner.UpdatedAt = entity.UpdatedAt;

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "{Repo} Upsert function error", typeof(UserRepository));
                return false;
            }
        }

        public override async Task<bool> Delete(int id)
        {
            try
            {
                var exist = await dbSet.Where(x => x.Id == id).FirstOrDefaultAsync();

                if (exist == null) return false;

                dbSet.Remove(exist);
                return true;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "{Repo} Delete function error", typeof(MasterScannerRepository));
                return false;
            }
        }
    }
}
