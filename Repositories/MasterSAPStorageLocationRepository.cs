﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS_RFID.Data;
using WMS_RFID.IRepositories;
using WMS_RFID.Models;

namespace WMS_RFID.Repositories
{
    public class MasterSAPStorageLocationRepository : GenericRepository<MasterSAPStorageLocation>, IMasterSAPStorageLocationRepository
    {
        public MasterSAPStorageLocationRepository(ApplicationDbContext context, ILogger _logger) : base(context, _logger)
        {

        }

        public override async Task<IEnumerable<MasterSAPStorageLocation>> All()
        {
            try
            {
                return await dbSet.ToListAsync();
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} All function error", e);
                return new List<MasterSAPStorageLocation>();
            }
        }

        public override async Task<bool> Upsert(MasterSAPStorageLocation entity)
        {
            try
            {
                var existingStorage = await dbSet.Where(x => x.Id == entity.Id).FirstOrDefaultAsync();

                if (existingStorage == null)
                    return await Add(entity);

                existingStorage.StorageLocation = entity.StorageLocation;
                existingStorage.PlantCode = entity.PlantCode;
                existingStorage.Description = entity.Description;
                existingStorage.Active = entity.Active;
                existingStorage.CreatedAt = entity.CreatedAt;
                existingStorage.UpdatedAt = entity.UpdatedAt;

                return true;
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} Upsert function error", typeof(MasterSAPStorageLocationRepository));
                return false;
            }
        }
        public override async Task<bool> Delete(int id)
        {
            try
            {
                var exist = await dbSet.Where(x => x.Id == id).FirstOrDefaultAsync();

                if (exist == null)
                    return false;

                dbSet.Remove(exist);
                return true;
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} Delete function error", typeof(MasterSAPStorageLocationRepository));
                return false;
            }
        }
    }
}
