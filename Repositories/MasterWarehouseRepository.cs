﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS_RFID.Configuration;
using WMS_RFID.Data;
using WMS_RFID.IRepositories;
using WMS_RFID.Models;

namespace WMS_RFID.Repositories
{
    public class MasterWarehouseRepository : GenericRepository<MasterWarehouse>, IMasterWarehouseRepository
    {
        public MasterWarehouseRepository(ApplicationDbContext context, ILogger _logger) : base(context, _logger)
        {

        }

        public override async Task<IEnumerable<MasterWarehouse>> All()
        {
            try
            {
                return await dbSet.ToListAsync();
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} All function error", typeof(MasterWarehouseRepository));
                return new List<MasterWarehouse>();
            }
        }

        public override async Task<bool> Upsert(MasterWarehouse entity)
        {
            try
            {
                var existingWarehouse = await dbSet.Where(x => x.Id == entity.Id).FirstOrDefaultAsync();

                if (existingWarehouse == null)
                    return await Add(entity);

                existingWarehouse.FactoryID = entity.FactoryID;
                existingWarehouse.CompanyName = entity.CompanyName;
                existingWarehouse.WarehouseName = entity.WarehouseName;
                existingWarehouse.GroupFactory = entity.GroupFactory;
                existingWarehouse.SAPPlantCode = entity.SAPPlantCode;
                existingWarehouse.UserID = entity.UserID;
                existingWarehouse.CreatedAt = entity.CreatedAt;
                existingWarehouse.UpdatedAt = entity.UpdatedAt;

                return true;
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} Upsert function error", typeof(MasterWarehouseRepository));
                return false;
            }
        }

        public override async Task<bool> Delete(int id)
        {
            try
            {
                var exist = await dbSet.Where(x => x.Id == id).FirstOrDefaultAsync();

                if (exist == null) return false;

                dbSet.Remove(exist);
                return true;
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} Delete function error", typeof(MasterWarehouseRepository));
                return false;
            }
        }
    }
}
