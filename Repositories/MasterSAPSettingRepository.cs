﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS_RFID.Data;
using WMS_RFID.IRepositories;
using WMS_RFID.Models;

namespace WMS_RFID.Repositories
{
    public class MasterSAPSettingRepository : GenericRepository<MasterSAPSetting>, IMasterSAPSettingRepository
    {
        public MasterSAPSettingRepository(ApplicationDbContext context, ILogger logger) : base(context, logger)
        {

        }

        public override async Task<IEnumerable<MasterSAPSetting>> All()
        {
            try
            {
                return await dbSet.ToListAsync();
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} All function error", typeof(MasterSAPSettingRepository));
                return new List<MasterSAPSetting>();
            }
        }

        public override async Task<bool> Upsert(MasterSAPSetting entity)
        {
            try
            {
                var existingSetting = await dbSet.Where(x => x.Id == entity.Id).FirstOrDefaultAsync();

                if (existingSetting == null)
                    return await Add(entity);

                existingSetting.ProgramName = entity.ProgramName;
                existingSetting.AppServerHost = entity.AppServerHost;
                existingSetting.SystemNumber = entity.SystemNumber;
                existingSetting.SystemID = entity.SystemID;
                existingSetting.SAPUser = entity.SAPUser;
                existingSetting.Password = entity.Password;
                existingSetting.Client = entity.Client;
                existingSetting.Language = entity.Language;
                existingSetting.PoolSize = entity.PoolSize;
                existingSetting.MaxPoolSize = entity.MaxPoolSize;
                existingSetting.IdleTimeOut = entity.IdleTimeOut;
                existingSetting.ServerRestartTime = entity.ServerRestartTime;
                existingSetting.UserID = entity.UserID;
                existingSetting.CreatedAt = entity.CreatedAt;
                existingSetting.UpdatedAt = entity.UpdatedAt;

                return true;
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} Upsert function error", typeof(MasterSAPSettingRepository));
                return false;
            }
        }

        public override async Task<bool> Delete(int id)
        {
            try
            {
                var exist = await dbSet.Where(x => x.Id == id).FirstOrDefaultAsync();

                if (exist == null)
                    return false;

                dbSet.Remove(exist);
                return true;
            }
            catch(Exception e)
            {
                _logger.LogError(e, "{Repo} Delete function error", typeof(MasterSAPSettingRepository));
                return false;
            }
        }
    }
}
