﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS_RFID.Configuration;
using WMS_RFID.Models;

namespace WMS_RFID.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MasterSAPVendorCodeController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public MasterSAPVendorCodeController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var vendor = await _unitOfWork.MasterSAPVendorCode.All();
            return Ok(vendor);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetVendor(int id)
        {
            var item = await _unitOfWork.MasterSAPVendorCode.GetById(id);

            if (item == null)
                return NotFound();

            return Ok(item);
        }

        [HttpPost]
        public async Task<IActionResult> CreateVendor(MasterSAPVendorCode vendor)
        {
            if (ModelState.IsValid)
            {
                //vendor.Id = Guid.NewGuid();

                await _unitOfWork.MasterSAPVendorCode.Add(vendor);
                await _unitOfWork.CompleteAsync();

                return CreatedAtAction("GetVendor", new { vendor.Id }, vendor);
            }
            return new JsonResult("Something went wrong") { StatusCode = 500 };
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteVendor(int id)
        {
            var item = await _unitOfWork.MasterSAPVendorCode.GetById(id);

            if (item == null) return BadRequest();

            await _unitOfWork.MasterSAPVendorCode.Delete(id);
            await _unitOfWork.CompleteAsync();

            return Ok(item);
        }
    }
}
