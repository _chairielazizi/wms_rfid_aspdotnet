﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS_RFID.Configuration;
using WMS_RFID.Models;

namespace WMS_RFID.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MasterScannerController : ControllerBase
    {
        //public IActionResult Index()
        //{
        //    return View();
        //}
        private readonly IUnitOfWork _unitOfWork;

        public MasterScannerController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var scanners = await _unitOfWork.MasterScanner.All();
            return Ok(scanners);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetScanner(int id)
        {
            var item = await _unitOfWork.MasterScanner.GetById(id);

            if (item == null)
                return NotFound();

            return Ok(item);
        }

        [HttpPost]
        public async Task<IActionResult> CreateScanner(MasterScanner scanner)
        {
            if (ModelState.IsValid)
            {
                //scanner.Id = Guid.NewGuid();

                await _unitOfWork.MasterScanner.Add(scanner);
                await _unitOfWork.CompleteAsync();

                return CreatedAtAction("GetScanner", new { scanner.Id }, scanner);
            }
            return new JsonResult("Something went wrong") { StatusCode = 500 };
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteScanner(int id)
        {
            var item = await _unitOfWork.MasterScanner.GetById(id);

            if (item == null)
                return BadRequest();

            await _unitOfWork.MasterScanner.Delete(id);
            await _unitOfWork.CompleteAsync();

            return Ok(item);
        }
    }
}
