﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS_RFID.Configuration;
using WMS_RFID.Models;

namespace WMS_RFID.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MasterSAPSettingController : ControllerBase
    {
        //public IActionResult Index()
        //{
        //    return View();
        //}
        private readonly IUnitOfWork _unitOfWork;

        public MasterSAPSettingController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var setting = await _unitOfWork.MasterSAPSetting.All();
            return Ok(setting);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetSetting(int id)
        {
            var item = await _unitOfWork.MasterSAPSetting.GetById(id);

            if (item == null)
                return NotFound();

            return Ok(item);
        }

        [HttpPost]
        public async Task<IActionResult> CreateSetting(MasterSAPSetting setting)
        {
            if (ModelState.IsValid)
            {
                //setting.Id = Guid.NewGuid();

                await _unitOfWork.MasterSAPSetting.Add(setting);
                await _unitOfWork.CompleteAsync();

                return CreatedAtAction("GetSetting", new { setting.Id }, setting);   
            }
            return new JsonResult("Something went wrong") { StatusCode = 500 };
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteProgram(int id)
        {
            var item = await _unitOfWork.MasterSAPSetting.GetById(id);

            if (item == null)
                return BadRequest();

            await _unitOfWork.MasterSAPSetting.Delete(id);
            await _unitOfWork.CompleteAsync();
            
            return Ok(item);
        }
    }
}
