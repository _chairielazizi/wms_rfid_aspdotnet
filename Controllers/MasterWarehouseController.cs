﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS_RFID.Configuration;
using WMS_RFID.Models;

namespace WMS_RFID.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MasterWarehouseController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public MasterWarehouseController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var warehouse = await _unitOfWork.MasterWarehouse.All();
            return Ok(warehouse);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetWarehouse(int id)
        {
            var item = await _unitOfWork.MasterWarehouse.GetById(id);

            if (item == null) return NotFound();

            return Ok(item);
        }

        [HttpPost]
        public async Task<IActionResult> CreateWarehouse(MasterWarehouse warehouse)
        {
            if (ModelState.IsValid)
            {
                //warehouse.Id = Guid.NewGuid();

                await _unitOfWork.MasterWarehouse.Add(warehouse);
                await _unitOfWork.CompleteAsync();

                return CreatedAtAction("GetWarehouse", new { warehouse.Id }, warehouse);
            }
            return new JsonResult("Something when wrong") { StatusCode = 500 };
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteWarehouse(int id)
        {
            var item = await _unitOfWork.MasterWarehouse.GetById(id);

            if (item == null) return BadRequest();

            await _unitOfWork.MasterWarehouse.Delete(id);
            await _unitOfWork.CompleteAsync();

            return Ok(item);
        }
    }
}
