﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS_RFID.Configuration;
using WMS_RFID.Models;

namespace WMS_RFID.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MasterSAPStorageLocationController : ControllerBase
    {
        //public IActionResult Index()
        //{
        //    return View();
        //}
        private readonly IUnitOfWork _unitOfWork;

        public MasterSAPStorageLocationController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var storages = await _unitOfWork.MasterSAPStorageLocation.All();
            return Ok(storages);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetStorage(int id)
        {
            var item = await _unitOfWork.MasterSAPStorageLocation.GetById(id);

            if (item == null)
                return NotFound();

            return Ok(item);
        }

        [HttpPost]
        public async Task<IActionResult> CreateStorage(MasterSAPStorageLocation storage)
        {
            if (ModelState.IsValid)
            {
                //storage.Id = Guid.NewGuid();

                await _unitOfWork.MasterSAPStorageLocation.Add(storage);
                await _unitOfWork.CompleteAsync();

                return CreatedAtAction("GetStorage", new { storage.Id}, storage);
            }
            return new JsonResult("Something went wrong") { StatusCode = 500 };
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteStorage(int id)
        {
            var item = await _unitOfWork.MasterSAPStorageLocation.GetById(id);

            if (item == null) return BadRequest();

            await _unitOfWork.MasterSAPStorageLocation.Delete(id);
            await _unitOfWork.CompleteAsync();

            return Ok(item);
        }
    }
}
