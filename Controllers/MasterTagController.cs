﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS_RFID.Configuration;
using WMS_RFID.Models;

namespace WMS_RFID.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MasterTagController : ControllerBase
    {
        //public IActionResult Index()
        //{
        //    return View();
        //}

        private readonly IUnitOfWork _unitOfWork;

        public MasterTagController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var tag = await _unitOfWork.MasterTag.All();
            return Ok(tag);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTag(int id)
        {
            var item = await _unitOfWork.MasterTag.GetById(id);

            if (item == null) return NotFound();

            return Ok(item);
        }

        [HttpPost]
        public async Task<IActionResult> CreateTag(MasterTag tag)
        {
            if (ModelState.IsValid)
            {
                //tag.Id = Guid.NewGuid();

                await _unitOfWork.MasterTag.Add(tag);
                await _unitOfWork.CompleteAsync();

                return CreatedAtAction("GetTag", new { tag.Id }, tag);
            }
            return new JsonResult("Something went wrong") { StatusCode = 500 };
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteTag(int id)
        {
            var item = await _unitOfWork.MasterTag.GetById(id);

            if (item == null)
                return BadRequest();

            await _unitOfWork.MasterTag.Delete(id);
            await _unitOfWork.CompleteAsync();

            return Ok(item);
        }
    }
}
