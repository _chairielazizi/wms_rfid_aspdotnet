﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS_RFID.Configuration;
using WMS_RFID.Models;

namespace WMS_RFID.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MasterProgramController : ControllerBase
    {
        //public IActionResult Index()
        //{
        //    return View();
        //}
        private readonly IUnitOfWork _unitOfWork;

        public MasterProgramController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var programs = await _unitOfWork.MasterProgram.All();
            return Ok(programs);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetProgram(int id)
        {
            var item = await _unitOfWork.MasterProgram.GetById(id);

            if (item == null)
                return NotFound();

            return Ok(item);
        }

        [HttpPost]
        public async Task<IActionResult> CreateProgram(MasterProgram program)
        {
            if (ModelState.IsValid)
            {
                //program.Id = Guid.NewGuid();

                await _unitOfWork.MasterProgram.Add(program);
                await _unitOfWork.CompleteAsync();

                return CreatedAtAction("GetProgram", new { program.Id }, program);
            }
            return new JsonResult("Something went wrong") { StatusCode = 500 };
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteProgram(int id)
        {
            var item = await _unitOfWork.MasterProgram.GetById(id);

            if (item == null)
                return BadRequest();

            await _unitOfWork.MasterProgram.Delete(id);
            await _unitOfWork.CompleteAsync();

            return Ok(item);
        }
    }
}
