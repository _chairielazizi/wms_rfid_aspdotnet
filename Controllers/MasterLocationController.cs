﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WMS_RFID.Configuration;
using WMS_RFID.Models;

namespace WMS_RFID.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MasterLocationController : ControllerBase
    {
        //public IActionResult Index()
        //{
        //    return View();
        //}

        private readonly IUnitOfWork _unitOfWork;

        public MasterLocationController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var locations = await _unitOfWork.MasterLocation.All();
            return Ok(locations);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetLocation(int id)
        {
            var item = await _unitOfWork.MasterLocation.GetById(id);

            if (item == null)
                return NotFound();

            return Ok(item);
        }

        [HttpPost]
        public async Task<IActionResult> CreateLocation (MasterLocation location)
        {
            if (ModelState.IsValid)
            {
                //location.Id = Guid.NewGuid();

                await _unitOfWork.MasterLocation.Add(location);
                await _unitOfWork.CompleteAsync();

                return CreatedAtAction("GetLocation", new { location.Id }, location);
            }
            return new JsonResult("Something went wrong") { StatusCode = 500 };
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteLocation(int id)
        {
            var item = await _unitOfWork.MasterLocation.GetById(id);

            if (item == null)
                return BadRequest();

            await _unitOfWork.MasterLocation.Delete(id);
            await _unitOfWork.CompleteAsync();

            return Ok(item);
        }
    }
}
